# scripts

Repository for my own personal script files. 

Sharing publicly for the benefit of all; adapt and use for your own purposes as you see fit, though please offer credit (even just a comment/URL link in source file) if you build something based on any of the material here.